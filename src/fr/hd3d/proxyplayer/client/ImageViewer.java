package fr.hd3d.proxyplayer.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.ui.Image;

public class ImageViewer extends Image implements IProxyPlayer
{

    public ImageViewer()
    {
        super();
        this.addLoadHandler(new LoadHandler() {
            
            @Override
            public void onLoad(LoadEvent event)
            {
                Element element = event.getRelativeElement();
                if (element == ImageViewer.this.getElement()) {
                    int originalHeight = ImageViewer.this.getOffsetHeight();
                    int originalWidth = ImageViewer.this.getOffsetWidth();
                    if (originalHeight > originalWidth) {
                        ImageViewer.this.setWidth("");
                    } else {
                        ImageViewer.this.setHeight("");
                    }
                }
            }
        });
    }
    
    @Override
    public void setSrc(String src)
    {
        this.setUrl(src);
    }
    
}

package fr.hd3d.proxyplayer.client.event;

import com.extjs.gxt.ui.client.event.EventType;


public class VideoPlayerEvent
{
    public static final EventType GOTO_START_EVENT = new EventType();
    public static final EventType REWIND_EVENT = new EventType();
    public static final EventType PLAY_EVENT = new EventType();
    public static final EventType FAST_FORWARD_EVENT = new EventType();
    public static final EventType GOTO_END_EVENT = new EventType();
    public static final EventType VIDEO_TIME_UPDATE_EVENT = new EventType();
    public static final EventType VIDEO_ENDED_EVENT = new EventType();
    public static final EventType PREV_FRAME_EVENT = new EventType();
    public static final EventType NEXT_FRAME_EVENT = new EventType();
    public static final EventType MARK_IN_EVENT = new EventType();
    public static final EventType MARK_OUT_EVENT = new EventType();
}

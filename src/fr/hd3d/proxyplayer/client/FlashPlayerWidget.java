package fr.hd3d.proxyplayer.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

public class FlashPlayerWidget extends Widget implements IProxyPlayer{
    
    private static final FlashPlayerImpl impl = GWT.create(FlashPlayerImpl.class);
    private String[] paramNames = new String[] { "movie", "allowFullScreen" };
    private String[] paramValues = new String[] { FlashPlayerImpl.PLAYER_URL_PARAM, "true" };
    
    public FlashPlayerWidget() {
        setElement(impl.createElement());
        this.setStyleName("html5player");
    }
    
    public FlashPlayerWidget(boolean debugMode) {
        this();
        impl.setDebugMode(debugMode);
    }
    
    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        impl.setPixelSize(width, height);
    }
    
    @Override
    public void setSrc(String proxyUrl)
    {
        if (proxyUrl != null)
        {
            impl.setData(impl.convertUrl(proxyUrl));
        }
        for (int i = 0; i < paramNames.length; i++)
        {
            String value = paramValues[i];
            if (FlashPlayerImpl.PLAYER_URL_PARAM.equals(value))
            {
                impl.createAndAppendParam(this, paramNames[i], impl.convertUrl(proxyUrl));
            }
            else
            {
                impl.createAndAppendParam(this, paramNames[i], value);
            }
        }
        impl.finish();
    }

}

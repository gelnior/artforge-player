package fr.hd3d.proxyplayer.client.util;

public enum ProxyMimeType
{

    VIDEO_MP4{
        @Override
        public String getType()
        {
            return "video/mp4";
        }
    },
    VIDEO_THREE_GPP
    {
        @Override
        public String getType()
        {
            return "video/3gpp";
        }
    },
    VIDEO_OGG
    {
        @Override
        public String getType()
        {
            return "video/ogg";
        }
    },
    VIDEO_MKV
    {
        @Override
        public String getType()
        {
            return "video/x-matroska";
        }
    },
    VIDEO_WEBM
    {
        @Override
        public String getType()
        {
            return "video/webm";
        }
    },
    IMAGE_GIF{
      @Override
        public String getType()
        {
            return "image/gif";
        }  
    },
    IMAGE_JPEG{
        @Override
        public String getType()
        {
            return "image/jpeg";
        }
    },
    IMAGE_PNG{
        @Override
        public String getType()
        {
            return "image/png";
        }
    }
    ;
    public abstract String getType();

}

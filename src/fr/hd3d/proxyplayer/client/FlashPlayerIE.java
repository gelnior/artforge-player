package fr.hd3d.proxyplayer.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Widget;


public class FlashPlayerIE extends FlashPlayerImpl
{

    private static final String VALUE_ATTRIBUTE_ID = "%value%";
    private static final String NAME_ATTRIBUTE_ID = "%name%";
    private static final String VALUE_ATTRIBUTE = "value=\"" + VALUE_ATTRIBUTE_ID + "\"";
    private static final String NAME_ATTRIBUTE = "name=\"" + NAME_ATTRIBUTE_ID + "\"";
    private static final String PARAM_ELEMENT = "<param " + NAME_ATTRIBUTE + " " + VALUE_ATTRIBUTE + "/>";

    private static final String PARAM_ELEMENT_ID = "%param%";
    private static final String OBJECT_ATTRIBUTE_ID = "%attribute%";
    private String innerHtml = "";
    private Widget widget;

    @Override
    public Element createElement()
    {
        return DOM.createDiv();
    }

    @Override
    public void setData(String proxyUrl)
    {
        this.innerHtml = "<object data=\"" + proxyUrl + "\" type=\"application/x-shockwave-flash\" "+ OBJECT_ATTRIBUTE_ID +">" + PARAM_ELEMENT_ID
                + "</object>";

    }
    
    @Override
    public void setPixelSize(int width, int height)
    {
        this.innerHtml = this.innerHtml.replaceAll(OBJECT_ATTRIBUTE_ID, "width=" + width + " " + OBJECT_ATTRIBUTE_ID);
        this.innerHtml = this.innerHtml.replaceAll(OBJECT_ATTRIBUTE_ID, "height=" + height + " " + OBJECT_ATTRIBUTE_ID);
        finish();
    }

    @Override
    public void createAndAppendParam(Widget widget, String name, String value)
    {
        this.widget = widget;
        String paramToAdd = PARAM_ELEMENT;
        paramToAdd = paramToAdd.replaceAll(NAME_ATTRIBUTE_ID, name);
        paramToAdd = paramToAdd.replaceAll(VALUE_ATTRIBUTE_ID, value);
        this.innerHtml = this.innerHtml.replaceAll(PARAM_ELEMENT_ID, paramToAdd + PARAM_ELEMENT_ID);
    }
    
    @Override
    public void finish()
    {
        String inner = this.innerHtml.replaceAll(PARAM_ELEMENT_ID, "");
        inner = inner.replaceAll(OBJECT_ATTRIBUTE_ID, "");
        this.widget.getElement().setInnerHTML(innerHtml);
    }
}

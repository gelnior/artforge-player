package fr.hd3d.proxyplayer.client.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.proxyplayer.client.VideoPlayer;
import fr.hd3d.proxyplayer.client.event.VideoPlayerEvent;


public class VideoPlayerController extends Controller
{
    private static final double DEFAULT_VIDEO_FRAME_RATE = 25;
    private final VideoPlayer player;
    public static final long ONE_MIN = 60;
    public static final long ONE_HOUR = 60 * ONE_MIN;
    private NumberFormat format = NumberFormat.getFormat("00");

    public VideoPlayerController(VideoPlayer player)
    {
        this.player = player;
        registerEvents();
    }

    private void registerEvents()
    {
        this.registerEventTypes(VideoPlayerEvent.GOTO_START_EVENT);
        this.registerEventTypes(VideoPlayerEvent.REWIND_EVENT);
        this.registerEventTypes(VideoPlayerEvent.PLAY_EVENT);
        this.registerEventTypes(VideoPlayerEvent.FAST_FORWARD_EVENT);
        this.registerEventTypes(VideoPlayerEvent.GOTO_END_EVENT);
        this.registerEventTypes(VideoPlayerEvent.NEXT_FRAME_EVENT);
        this.registerEventTypes(VideoPlayerEvent.PREV_FRAME_EVENT);
        this.registerEventTypes(VideoPlayerEvent.MARK_IN_EVENT);
        this.registerEventTypes(VideoPlayerEvent.MARK_OUT_EVENT);
        // video events
        this.registerEventTypes(VideoPlayerEvent.VIDEO_TIME_UPDATE_EVENT);
        this.registerEventTypes(VideoPlayerEvent.VIDEO_ENDED_EVENT);
        this.registerEventTypes(CommonEvents.START);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (VideoPlayerEvent.GOTO_START_EVENT.equals(event.getType()))
        {
            gotoStart();
        }
        else if (VideoPlayerEvent.GOTO_END_EVENT.equals(event.getType()))
        {
            gotoEnd();
        }
        else if (VideoPlayerEvent.PLAY_EVENT.equals(event.getType()))
        {
            onPlayPause();
        }
        else if (VideoPlayerEvent.FAST_FORWARD_EVENT.equals(event.getType()))
        {
            goFastFoward();
        }
        else if (VideoPlayerEvent.REWIND_EVENT.equals(event.getType()))
        {
            goRewind();
        }
        else if (VideoPlayerEvent.VIDEO_TIME_UPDATE_EVENT.equals(event.getType()))
        {
            onVideoTimeUpdate();
            updateTimeCode();
        }
        else if (VideoPlayerEvent.VIDEO_ENDED_EVENT.equals(event.getType()))
        {
            onVideoEnded();
        }
        else if (VideoPlayerEvent.NEXT_FRAME_EVENT.equals(event.getType()))
        {
            onNextFrame();
        }
        else if (VideoPlayerEvent.PREV_FRAME_EVENT.equals(event.getType()))
        {
            onPrevFrame();
        }
        else if (VideoPlayerEvent.MARK_IN_EVENT.equals(event.getType()))
        {
            onMarkedIn();
        }
        else if (VideoPlayerEvent.MARK_OUT_EVENT.equals(event.getType()))
        {
            onMarkedOut();
        }
        else if (CommonEvents.START.equals(event.getType()))
        {
            onStart();
        }
    }

    private void onStart()
    {
        ServicesPath.initPath();
        ServicesModelType.initModelTypes();
        
    }

    private void onMarkedOut()
    {

    }

    private void onMarkedIn()
    {

    }

    private void onPrevFrame()
    {
        addOrRemoveFrame(-1);
    }

    private void addOrRemoveFrame(int nbFrame)
    {
        double currentTime = player.getVideoHTML().getCurrentTime();
        currentTime += (1 / DEFAULT_VIDEO_FRAME_RATE) * nbFrame;
        player.getVideoHTML().setCurrentTime(currentTime);
    }

    private void onNextFrame()
    {
        addOrRemoveFrame(1);
    }

    private void gotoEnd()
    {
        double duration = player.getVideoHTML().getDuration();
        player.getVideoHTML().setCurrentTime(duration);
        player.setFocus(true);
    }

    private void goRewind()
    {
        double currentRate = player.getVideoHTML().getPlaybackRate();
        GWT.log("currentRate = " + currentRate, null);
        player.getVideoHTML().setDefaultPlaybackRate(currentRate - 1);
        player.getVideoHTML().setPlaybackRate(currentRate - 1);
        player.setFocus(true);
    }

    private void goFastFoward()
    {
        double currentRate = player.getVideoHTML().getPlaybackRate();
        GWT.log("currentRate = " + currentRate, null);
        player.getVideoHTML().setDefaultPlaybackRate(currentRate + 1);
        player.getVideoHTML().setPlaybackRate(currentRate + 1);
        player.setFocus(true);
    }

    private void updateTimeCode()
    {
        double time = player.getVideoHTML().getCurrentTime();
        int nbHours = ((int) (time / (ONE_HOUR)));
        time = time - (nbHours * ONE_HOUR);
        int nbMin = (int) (time / (ONE_MIN));
        time = time - (nbMin * ONE_MIN);
        String timeCodeString = format.format(nbHours) + ":" + format.format(nbMin) + ":" + format.format(time);
        player.getTimeCodeLabel().setText(timeCodeString);
    }

    private void onVideoEnded()
    {
        player.getPlayButton().setIcon(AbstractImagePrototype.create(player.getResources().videoPlay()));
        player.getProgressBar().updateProgress(0, null);
        player.getVideoHTML().setCurrentTime(0);
        player.getVideoHTML().playPause();
    }

    private void onVideoTimeUpdate()
    {
        double value = player.getVideoHTML().getCurrentTime() / player.getVideoHTML().getDuration();
        player.getProgressBar().updateProgress(value, null);
    }

    private void onPlayPause()
    {
        if (player.getVideoHTML().isPaused())
        {
            player.getPlayButton().setIcon(AbstractImagePrototype.create(player.getResources().videoPause()));
        }
        else
        {
            player.getPlayButton().setIcon(AbstractImagePrototype.create(player.getResources().videoPlay()));
        }
        player.getVideoHTML().playPause();
        player.setFocus(true);
    }

    private void gotoStart()
    {
        player.getVideoHTML().setCurrentTime(0);
        player.setFocus(true);
    }
}

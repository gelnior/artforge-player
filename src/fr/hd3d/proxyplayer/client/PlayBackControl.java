package fr.hd3d.proxyplayer.client;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.event.EventDispatcher;


public class PlayBackControl extends Button
{
    public PlayBackControl(final EventType eventType, String tooltip, ImageResource icon)
    {
        super();
        setIcon(AbstractImagePrototype.create(icon));
        setToolTip(tooltip);
        addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                EventDispatcher.forwardEvent(eventType);
            }
        });
    }
}

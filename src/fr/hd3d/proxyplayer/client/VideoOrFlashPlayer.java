package fr.hd3d.proxyplayer.client;

import java.util.List;

import fr.hd3d.html5.video.client.VideoSource;
import fr.hd3d.html5.video.client.VideoWidget;
import fr.hd3d.html5.video.client.VideoSource.VideoType;

public class VideoOrFlashPlayer extends VideoWidget
{
    public VideoOrFlashPlayer(boolean autoPlay, boolean controls, String poster)
    {
        super(autoPlay, controls, poster);
    }

    @Override
    public void setSources(List<VideoSource> sources)
    {
        super.setSources(sources);
        addEmbedElement(sources);
    }
    
    private void addEmbedElement(List<VideoSource> sources)
    {
        VideoSource MP4Source = null;
        for (VideoSource videoSource : sources)
        {
            if (videoSource.getVideoType().equals(VideoType.MP4)) {
                MP4Source = videoSource;
                break;
            }
        }
        if (MP4Source != null) {
            FlashPlayerWidget objectPlayerWidget = new FlashPlayerWidget();
            objectPlayerWidget.setSrc(MP4Source.getSrc());
            this.getElement().appendChild(objectPlayerWidget.getElement());
        }
    }
}

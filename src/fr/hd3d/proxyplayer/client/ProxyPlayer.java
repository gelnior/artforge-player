package fr.hd3d.proxyplayer.client;

import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.html5.video.client.VideoWidget.TypeSupport;
import fr.hd3d.proxyplayer.client.util.ProxyMimeType;


public class ProxyPlayer extends FocusPanel
{
    private IProxyPlayer displayer;

    public ProxyPlayer(ProxyModelData proxyModelData)
    {
        if (isVideoType(proxyModelData))
        {
            try
            {
                displayer = new VideoPlayer(false, null, VideoPlayer.getDefaultResources());
                if (!isTypeSupported(proxyModelData)) {
                    displayer = new FlashPlayerWidget(true);
                }
            }
            catch (Exception e)
            {
                displayer = new FlashPlayerWidget(true);
            }
        }
        else if (isImage(proxyModelData))
        {
            displayer = new ImageViewer();
        }

        if (displayer != null && displayer instanceof Widget)
        {
            displayer.setSrc(proxyModelData.getProxyPath());
            this.setWidget((Widget) displayer);
        }
    }

    private boolean isTypeSupported(ProxyModelData proxyModelData)
    {
        TypeSupport canPlayType = ((VideoPlayer) displayer).getVideoHTML().canPlayType(proxyModelData.getProxyTypeOpenType());
        return !TypeSupport.NO.equals(canPlayType);
    }

    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        if (displayer != null && displayer instanceof Widget)
        {
            ((Widget) displayer).setPixelSize(width, height);
        }
    }

    private boolean isImage(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.IMAGE_GIF.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_JPEG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_PNG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());

    }

    private boolean isVideoType(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.VIDEO_MKV.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_MP4.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_OGG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_THREE_GPP.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_WEBM.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());
    }
}

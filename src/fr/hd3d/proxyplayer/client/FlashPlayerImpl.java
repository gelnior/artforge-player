package fr.hd3d.proxyplayer.client;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ObjectElement;
import com.google.gwt.dom.client.ParamElement;
import com.google.gwt.user.client.ui.Widget;


public class FlashPlayerImpl
{
    public static final String URL_PARAM = "%url%";
    public static final String PLAYER_NAME = "player.swf";
    public static final String DEBUG_URL_PARAM = "%debug%";
    public static final String PLAYER_URL_PARAM = PLAYER_NAME + "?url=" + URL_PARAM + "&debug=" + DEBUG_URL_PARAM;
    private ObjectElement objectElement;
    private boolean debugMode = false;

    public boolean isDebugMode()
    {
        return debugMode;
    }

    public void setDebugMode(boolean debugMode)
    {
        this.debugMode = debugMode;
    }
    
    public ObjectElement getObjectElement()
    {
        return objectElement;
    }
    
    public Element getElement()
    {
        return objectElement;
    }
    
    public Element createElement()
    {
        objectElement = Document.get().createObjectElement();
        objectElement.setType("application/x-shockwave-flash");
        objectElement.setData(PLAYER_NAME);
        return objectElement;
    }
   
    public void setData(String proxyUrl) {
        objectElement.setData(proxyUrl);
    }

    public void createAndAppendParam(Widget widget, String name, String value)
    {
        ParamElement paramElement = ParamElement.as(Document.get().createParamElement());
        paramElement.setName(name);
        if (value != null)
        {
            paramElement.setValue(value);
            objectElement.insertFirst(paramElement);
        }
    }

    public String convertUrl(String proxyUrl)
    {
        return PLAYER_URL_PARAM.replaceAll(URL_PARAM, proxyUrl).replaceAll(DEBUG_URL_PARAM,
                String.valueOf(this.debugMode));
    }

    public void finish()
    {
    }

    public void setPixelSize(int width, int height)
    {
    }

}

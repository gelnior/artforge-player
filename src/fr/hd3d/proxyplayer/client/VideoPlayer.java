package fr.hd3d.proxyplayer.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.ProgressBar;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteData;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.html5.video.client.events.VideoEndedEvent;
import fr.hd3d.html5.video.client.events.VideoTimeUpdateEvent;
import fr.hd3d.html5.video.client.handlers.VideoEndedHandler;
import fr.hd3d.html5.video.client.handlers.VideoTimeUpdateHandler;
import fr.hd3d.proxyplayer.client.controller.VideoPlayerController;
import fr.hd3d.proxyplayer.client.event.VideoPlayerEvent;


public class VideoPlayer extends FocusPanel implements IProxyPlayer
{
    private static Resources DEFAULT_RESOURCES;
    private Resources resources;
    protected VideoOrFlashPlayer videoHTML;
    protected PlayBackControl playButton;
    protected Button startButton;
    // private Button rewindButton;
    // private Button fastForwardButton;
    protected Button endButton;
    protected Button markInButton;
    protected Button markOutButton;
    protected VideoPlayerController videoPlayerController;
    protected ProgressBar progressBar;
    protected Label timeCodeLabel;
    protected ContentPanel contentPanel;
    protected List<Widget> children;

    public static Resources getDefaultResources()
    {
        if (DEFAULT_RESOURCES == null)
        {
            DEFAULT_RESOURCES = GWT.create(Resources.class);
        }
        return DEFAULT_RESOURCES;
    }

    public static interface Resources extends ClientBundle
    {
        ImageResource videoEnd();

        ImageResource videoFastForward();

        ImageResource videoPause();

        ImageResource videoPlay();

        ImageResource videoRepeat();

        ImageResource videoRewind();

        ImageResource videoStart();
        
        ImageResource markIn();

        ImageResource markOut();
        
    }

    public VideoPlayer()
    {
        this(false, null, getDefaultResources());
    }

    public VideoPlayer(boolean autoPlay, String poster, Resources resources)
    {
        super();
        this.resources = resources;
        this.children = new ArrayList<Widget>();
        videoHTML = new VideoOrFlashPlayer(autoPlay, (this.resources == null), poster);
        contentPanel = new ContentPanel(new AbsoluteLayout());
        // contentPanel.setHeaderVisible(false);
        videoHTML.getElement().getStyle().setWidth(100, Unit.PCT);
        videoHTML.getElement().getStyle().setHeight(100, Unit.PCT);
        if (this.resources != null)
        {
            createPlayBackControlsBar();
        }
        AbsoluteData absoluteData = new AbsoluteData(0, 0);
        contentPanel.add(videoHTML, absoluteData);
        addVideoEvents();
        this.add(contentPanel);
        videoPlayerController = new VideoPlayerController(this);
        EventDispatcher.get().addController(videoPlayerController);
        TextField<String> textField = new TextField<String>();
        textField.addKeyListener(new KeyListener());
    }

    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        this.contentPanel.setPixelSize(width, height);
    }

    protected void addVideoEvents()
    {
        videoHTML.addTimeUpdateHandler(new VideoTimeUpdateHandler() {
            @Override
            public void onTimeUpdated(VideoTimeUpdateEvent event)
            {
                EventDispatcher.forwardEvent(VideoPlayerEvent.VIDEO_TIME_UPDATE_EVENT);
            }
        });
        videoHTML.addEndedHandler(new VideoEndedHandler() {
            @Override
            public void onVideoEnded(VideoEndedEvent event)
            {
                EventDispatcher.forwardEvent(VideoPlayerEvent.VIDEO_ENDED_EVENT);
            }
        });
        progressBar.addListener(Events.OnMouseDown, new Listener<ComponentEvent>() {

            @Override
            public void handleEvent(ComponentEvent be)
            {
                double eventX = be.getClientX();
                double pbX = progressBar.getAbsoluteLeft();
                double pbWidth = progressBar.getOffsetWidth();
                double progressRatio = (eventX - pbX) / pbWidth;
                double totalTime = videoHTML.getDuration();
                double seekableTime = totalTime * progressRatio;
                videoHTML.setCurrentTime(seekableTime);
            }
        });
        progressBar.sinkEvents(Events.OnMouseDown.getEventCode());
        this.addKeyDownHandler(new KeyDownHandler() {

            @Override
            public void onKeyDown(KeyDownEvent event)
            {
                if (videoHTML.isPaused())
                {
                    if (event.getNativeKeyCode() == KeyCodes.KEY_RIGHT)
                    {
                        // right key press
                        EventDispatcher.forwardEvent(VideoPlayerEvent.NEXT_FRAME_EVENT);
                    }
                    else if (event.getNativeKeyCode() == KeyCodes.KEY_LEFT)
                    {
                        // left key press
                        EventDispatcher.forwardEvent(VideoPlayerEvent.PREV_FRAME_EVENT);
                    }
                }
                if (event.getNativeKeyCode() == 32)
                {
                    // space bar press
                    EventDispatcher.get().dispatch(VideoPlayerEvent.PLAY_EVENT);
                }
            }
        });
    }

    private void createPlayBackControlsBar()
    {
        new FillToolItem();
        ToolBar toolBar = new ToolBar();
        progressBar = new ProgressBar();
        timeCodeLabel = new Label("00:00");
        startButton = new PlayBackControl(VideoPlayerEvent.GOTO_START_EVENT, "Start", this.resources.videoStart());
        // rewindButton = new PlayBackControl(VideoPlayerEvent.REWIND_EVENT, "Rewind", this.resources.videoRewind());
        playButton = new PlayBackControl(VideoPlayerEvent.PLAY_EVENT, "Play/Pause", this.resources.videoPlay());
        // fastForwardButton = new PlayBackControl(VideoPlayerEvent.FAST_FORWARD_EVENT, "Fast forward", this.resources
        // .videoFastForward());
        endButton = new PlayBackControl(VideoPlayerEvent.GOTO_END_EVENT, "End", this.resources.videoEnd());
        markInButton = new PlayBackControl(VideoPlayerEvent.MARK_IN_EVENT, "Mark In", this.resources.markIn());
        markOutButton = new PlayBackControl(VideoPlayerEvent.MARK_OUT_EVENT, "Mark Out", this.resources.markOut());
        toolBar.add(startButton);
        // toolBar.add(rewindButton);
        toolBar.add(playButton);
        // toolBar.add(fastForwardButton);
        toolBar.add(endButton);
        toolBar.add(new SeparatorToolItem());
        toolBar.add(markInButton);
        toolBar.add(markOutButton);
        toolBar.add(new SeparatorToolItem());
        toolBar.add(progressBar);
        toolBar.add(timeCodeLabel);
        contentPanel.setBottomComponent(toolBar);
        DeferredCommand.addCommand(new Command() {
            @Override
            public void execute()
            {
                Element progressBarParentElement = progressBar.getElement().getParentElement();
                progressBarParentElement.getStyle().setWidth(100, Unit.PCT);
                progressBar.getElement().getStyle().setWidth(99, Unit.PCT);
                progressBar.getElement().getStyle().setHeight(10, Unit.PX);
            }
        });
    }

    public PlayBackControl getPlayButton()
    {
        return playButton;
    }

    public Resources getResources()
    {
        return resources;
    }

    public ProgressBar getProgressBar()
    {
        return progressBar;
    }

    public Label getTimeCodeLabel()
    {
        return timeCodeLabel;
    }

    @Override
    public void add(Widget w)
    {
        // Detach new child.
        if (w != null)
        {
            w.removeFromParent();
        }

        children.add(w);

        if (w != null)
        {
            // Physical attach.
            DOM.appendChild(getContainerElement(), w.getElement());

            adopt(w);
        }
    }

    @Override
    public Iterator<Widget> iterator()
    {
        return children.iterator();
    }

    @Override
    public boolean remove(Widget w)
    {
        if (!children.contains(w))
        {
            return false;
        }
        // Orphan.
        try
        {
            orphan(w);
        }
        finally
        {
            // Physical detach.
            getContainerElement().removeChild(w.getElement());
            children.remove(w);
        }
        return true;
    }

    @Override
    public void setWidget(Widget w)
    {
        throw new IllegalStateException("SimplePanel can only contain one child widget");
    }

    public ContentPanel getContentPanel()
    {
        return contentPanel;
    }

    public VideoOrFlashPlayer getVideoHTML()
    {
        return videoHTML;
    }
    
    @Override
    public void setSrc(String src)
    {
        this.videoHTML.addSource(src);
    }

}
